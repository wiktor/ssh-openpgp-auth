// SPDX-FileCopyrightText: 2023 Wiktor Kwapisiewicz <wiktor@metacode.biz>
// SPDX-FileCopyrightText: David Runge <dave@sleepmap.de>
// SPDX-License-Identifier: Apache-2.0 OR MIT
use fqdn::FQDN;
use rstest::rstest;
use sshd_openpgp_auth::create_trust_anchor;
use testresult::TestResult;

mod common;
use common::default_host;
use common::Error;

#[rstest]
fn test_create_trust_anchor(default_host: Result<FQDN, Error>) -> TestResult {
    let default_host = default_host?;
    let cert = create_trust_anchor(&default_host, None, None)?;
    assert!(cert.is_tsk(), "Cert didn't contain secret bits.");
    assert!(
        cert.keys().subkeys().next().is_none(),
        "Trust anchor should not contain any subkeys."
    );
    let user_ids = cert.userids().collect::<Vec<_>>();
    assert_eq!(user_ids.len(), 1, "Cert should contain a single User ID");
    assert_eq!(
        format!("<ssh-openpgp-auth@{}>", &default_host),
        String::from_utf8_lossy(user_ids[0].value()),
    );
    Ok(())
}
