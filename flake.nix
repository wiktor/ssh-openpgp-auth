# SPDX-FileCopyrightText: 2024 Doron Behar <doron.behar@gmail.com>
# SPDX-License-Identifier: CC0-1.0
{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self
  , nixpkgs
  , utils
  }: utils.lib.eachDefaultSystem (system: let
    pkgs = import nixpkgs { inherit system; };
  in {
    overlays = final: prev: {
      inherit (self.packages.${system})
        ssh-openpgp-auth
        sshd-openpgp-auth
      ;
    };
    packages = {
      # Besides the `src`, `cargoTomlPkg` and `pname` arguments, this
      # package.nix could be copied almost as is to Nixpkgs'
      # pkgs/by-name/ss/ssh{d,}-openpgp-auth/package.nix files, and should be
      # maintained in parallel to this local version of it.
      ssh-openpgp-auth = pkgs.callPackage ./package.nix {
        pname = "ssh-openpgp-auth";
        src = self;
        cargoTomlPkg = (builtins.fromTOML (builtins.readFile ./ssh-openpgp-auth/Cargo.toml)).package;
      };
      sshd-openpgp-auth = pkgs.callPackage ./package.nix {
        pname = "sshd-openpgp-auth";
        src = self;
        cargoTomlPkg = (builtins.fromTOML (builtins.readFile ./sshd-openpgp-auth/Cargo.toml)).package;
      };
    };
    defaultPackage = self.packages.${system}.ssh-openpgp-auth;
    devShell = with pkgs; mkShell {
      nativeBuildInputs = [
        cargo
        rustc
        rustfmt
        rustPackages.clippy
        just
        rust-script
        codespell
        reuse
      ];
      RUST_SRC_PATH = rustPlatform.rustLibSrc;
    };
  });
}
