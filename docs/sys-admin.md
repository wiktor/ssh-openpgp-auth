<!--
SPDX-FileCopyrightText: 2024- Wiktor Kwapisiewicz <wiktor@metacode.biz>
SPDX-License-Identifier: CC0-1.0
-->

# System administrator's guide

In this guide we will introduce the `sshd-openpgp-auth` command-line application and walk through the initial setup.

## Requirements

The guide assumes the following:

  - `sshd-openpgp-tool` has been installed (see the project README.md document),
  - the SSH host has been provisioned already (OpenSSH is installed and host keys are present),
  - the guide uses `example.com` as the DNS name of the target SSH host.

## Generating the host certificate

SSH OpenPGP Authenticator's design is centered around the concept of a "host certificate" which serves as a permanent identity for the host.
The certificate is implemented as a single OpenPGP certificate containing all [SSH host keys](https://man.archlinux.org/man/core/openssh/sshd.8.en#AUTHENTICATION) as [authentication subkeys](https://openpgp.dev/book/glossary.html#term-Authentication-Key-Flag).

The first step from the administrator's perspective would be the creation of the OpenPGP host certificate for the target machine:

```sh
sshd-openpgp-auth init "example.com"
```

The new [Transferable Secret Key (TSK)](https://openpgp.dev/book/glossary.html#term-Transferable-Secret-Key) (by default stored below `/var/lib/sshd-openpgp-auth/`) does not yet have any authentication subkeys.

## Adding SSH keys

Before the host certificate can be used by clients it needs to be populated
with SSH keys.

### Appending local keys

In a lightweight setup where the managed host certificate and SSH keys live
on the same machine adding local keys is the right solution and it's as easy as:

```sh
sshd-openpgp-auth add
```

The above command adds all available local SSH host keys as authentication subkeys to the newly created OpenPGP host certificate.

### Appending remote keys

In more elaborate use-cases one team may manage the SSH hosts while another takes care of the OpenPGP host certificates for the hosts.
The second team may be provided with the public SSH host keys via an out-of-band medium (e.g. on a signed and encrypted media).

```sh
sshd-openpgp-auth add --known-hosts < keys
```

The `ssh-keyscan` utility (part of the OpenSSH project) provides output in known_hosts format as well and can therefore be used directly:

```sh
ssh-keyscan -t ecdsa,ed25519,rsa example.com | sshd-openpgp-auth add --known-hosts
```

## Making the host certificate accessible

The primary method for delivering the host certificate to end-users is Web Key Directory (WKD).
This is not strictly required, and there are workflows where the host certificate would be delivered out-of-band.
Still, WKD offers a streamlined experience for the end-users as the host certificate is downloaded automatically.

A WKD is usually exposed by an HTTP server and `sshd-openpgp-auth` provides the necessary integration to directly generate the necessary directory structure:

```sh
sshd-openpgp-auth export "example.com"
```

## Maintenance

From time to time the host certificate needs to be maintained.

### Expiry

The certificate is created with a finite expiry date, that needs to be prolonged to indicate that the certificate is still in use and valid.

The following command can be used in a systemd timer or a cron job since when the certificate is fresh it's a no-op:

```sh
sshd-openpgp-auth extend --threshold 365 --expiry 730
```

### Revocations

The `add` subcommand always appends the SSH keys and never removes them.
Due to the append-only nature of OpenPGP certificates an explicit action is needed if a subkey usage has to be discontinued.

The most probable scenario where this is important is key rollover (e.g. due to switching physical hosts):

```sh
sshd-openpgp-auth revoke --reason superseded --subkey-fingerprint 12345678901234567890 --message "Physical host has been replaced."
```

More in depth documentation on revocations can be found in the [`sshd-openpgp-auth`'s README](../sshd-openpgp-auth/README.md#revoking-authentication-subkeys).
