# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.3](https://codeberg.org/wiktor/ssh-openpgp-auth/compare/ssh-openpgp-auth-v0.2.2...ssh-openpgp-auth-v0.2.3) - 2025-01-16

### Fixed

- Update link to project page on nlnet.nl

### Other

- Move reuse annotations from changelogs to REUSE.toml
- Fix README for ssh-openpgp-auth
- Update dependencies
- Add vale checking and fix minor wording issues

## [0.2.2](https://codeberg.org/wiktor/ssh-openpgp-auth/compare/ssh-openpgp-auth-v0.2.1...ssh-openpgp-auth-v0.2.2) - 2024-02-29

### Fixed
- *(ssh-openpgp-auth)* Disregard expired and revoked host certificates
- *(ssh-openpgp-auth)* Simplify logic for getting certs from cert store
- *(ssh-openpgp-auth)* Do not consider revoked authentication subkeys

### Other
- Use NLNet's template in the "Funding/Sponsors" section

## [0.2.1](https://codeberg.org/wiktor/ssh-openpgp-auth/compare/ssh-openpgp-auth-v0.2.0...ssh-openpgp-auth-v0.2.1) - 2024-02-10

### Other
- update Cargo.lock dependencies
